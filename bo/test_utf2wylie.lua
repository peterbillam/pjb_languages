#!/usr/bin/env lua
---------------------------------------------------------------------
--     This Lua5 script is Copyright (c) 2021, Peter J Billam      --
--                         pjb.com.au                              --
--  This script is free software; you can redistribute it and/or   --
--         modify it under the same terms as Lua5 itself.          --
---------------------------------------------------------------------
local Version = '1.0  for Lua5'
local VersionDate  = '25jun2021'
local Synopsis = [[
test_utf2wylie.lua [options] [filenames]
]]
---------------------------------------------------------------------
local function split(s, pattern, maxNb) -- http://lua-users.org/wiki/SplitJoin
    if not s or string.len(s)<2 then return {s} end
    if not pattern then return {s} end
    if maxNb and maxNb <2 then return {s} end
    local result = { }
    local theStart = 1
    local theSplitStart,theSplitEnd = string.find(s,pattern,theStart)
    local nb = 1
    while theSplitStart do
        table.insert( result, string.sub(s,theStart,theSplitStart-1) )
        theStart = theSplitEnd + 1
        theSplitStart,theSplitEnd = string.find(s,pattern,theStart)
        nb = nb + 1
        if maxNb and nb >= maxNb then break end
    end
    table.insert( result, string.sub(s,theStart,-1) )
    return result
end
function warn(...)
    local a = {}
    for k,v in pairs{...} do table.insert(a, tostring(v)) end
    io.stderr:write(table.concat(a),'\n') ; io.stderr:flush()
end
function die(...) warn(...);  os.exit(1) end

local eps = .000000001
function equal(x, y)   -- print('#x='..#x..' #y='..#y)
    if #x ~= #y then return false end
    local i; for i=1,#x do
        if math.abs(x[i]-y[i]) > eps then return false end
    end
    return true
end
local Test = 12 ; local i_test = 0; local Failed = 0;
function ok(b,s)
    i_test = i_test + 1
    if b then
        io.write('ok '..i_test..' - '..s.."\n")
        return true
    else
        io.write('not ok '..i_test..' - '..s.."\n")
        Failed = Failed + 1
        return false
    end
end
---------------------------------------------------------------------
-- BUGs: mtha' mjug - end, gyal ba karma pa - Karmapa
local u = "\u{0F58}\u{0F50}\u{0F60}\u{0F0B}\u{0F58}\u{0F47}\u{0F74}\u{0F42}"
local correct = "mtha' mjug"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "mtha' mjug - end") then
    warn('  was: '..rv..' should be: '..correct)
end

u = "\u{0F51}\u{0F42}\u{0F60}\u{0F0B}\u{0F62}\u{0F7C}\u{0F42}\u{0F66}"
correct = "dga' rogs"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "dga' rogs - boyfriend") then
    warn('  was: '..rv..' should be: '..correct)
end

u = "\u{0F62}\u{0F92}\u{0FB1}\u{0F63}\u{0F0B}\u{0F56}\u{0F0B}\u{0F40}\u{0F62}\u{0FA8}\u{0F0B}\u{0F54}"
correct = "rgyal ba karma pa"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "rgyal ba karma pa - Karmapa") then
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F49}\u{0F7A}\u{0F53}\u{0F0B}\u{0F42}\u{0F0B}\u{0F58}\u{0F7A}\u{0F51}\u{0F0B}\u{0F54}\u{0F60}\u{0F72}"
correct = "nyen ga med pa'i"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "nyen ga med pa'i - safe (adj)") then
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F42}\u{0F0B}\u{0F62}\u{0F7A}\u{0F0B}\u{0F61}\u{0F72}\u{0F53}\u{0F0B}\u{0F53}\u{0F60}\u{0F72}"
correct = "ga re yin na'i"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "ga re yin na'i - whatever") then
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F41}\u{0F60}\u{0F72}\u{0F0B}\u{0F49}\u{0F72}\u{0F53}\u{0F0B}\u{0F58}"
correct = "kha'i nyin ma"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "kha'i nyin ma - day before yesterday") then
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F60}\u{0F51}\u{0F72}\u{0F62}"
correct = "'dir"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "'dir - here") then
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F66}\u{0F74}\u{0F60}\u{0F72}"
correct = "su'i"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "su'i - whose?, whose") then
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F60}\u{0F7C}\u{0F42}\u{0F0B}\u{0F63}"
correct = "'og la"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "'og la - below") then
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F62}\u{0F92}\u{0F7C}\u{0F51}\u{0F0B}\u{0F58}"
correct = "rgod ma"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "rgod ma - mare") then
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F66}\u{0FA1}\u{0F74}\u{0F42}\u{0F0B}\u{0F64}\u{0F7C}\u{0F66}"
correct = "sdug shos"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "sdug shos - worst") then
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F66}\u{0F7A}\u{0F58}\u{0F66}\u{0F0B}\u{0F42}\u{0F74}\u{0F0B}\u{0F61}\u{0F44}\u{0F66}\u{0F0B}\u{0F54}\u{0F7C}"
correct = "sems gu yangs po"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
-- XXX PROBLEM sngas mgo - pillow, but NOT lngas = awake, or yngas !!
if not ok(rv == correct, "sems gu yangs po - carefree") then
	warn("  y, ng and s are separate characters")
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F66}\u{0F94}\u{0F66}\u{0F0B}\u{0F58}\u{0F42}\u{0F7C}"
correct = "sngas mgo"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "sngas mgo - pillow") then
	warn("  ng is a subscript to s !")
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F63}\u{0F44}\u{0F66}"
correct = "langs"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "langs - awake") then
	warn("  l, ng and s are separate characters")
    warn('  was: '..rv..'   should be: '..correct)
end

u = "\u{0F42}\u{0F5F}\u{0F60}\u{0F0B}\u{0F60}\u{0F41}\u{0F7C}\u{0F62}"
correct = "gza' 'khor"
local P = io.popen("echo "..u.." | utf2wylie", 'r')
local rv = P:read('l') ; P:close()
if not ok(rv == correct, "gza' 'khor - week") then
    warn('  was: '..rv..'   should be: '..correct)
end

if     Failed == 0 then print("passed all tests")
elseif Failed == 1 then print("failed 1 test")
else print("failed "..tonumber(Failed).." tests")
end



