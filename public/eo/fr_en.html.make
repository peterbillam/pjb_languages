#! /usr/bin/perl
# <MAP><AREA TABINDEX=1 RECT="10,10,100,100" ID="terd_area"></MAP>
# <FORM><INPUT TYPE="button" TABINDEX=2 NAME="term_area" ID="term_area"></FORM>
# <TEXTAREA TABINDEX=3 NAME="worm_area" ID="worm_area">
# hi
# </TEXTAREA>
sub datestamp {
   my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime;
   sprintf ("%4.4d%2.2d%2.2d", $year+1900, $mon+1, $mday);
}

print <<'EOT';
<HTML><HEAD><TITLE>Esperanto-English</TITLE>
EOT
system '/home/pbin/fr_en -h -j';
print <<'EOT';
</HEAD><BODY BGCOLOR="#FFFFFF">
<H1><CENTER><A NAME="top">French&lt;-&gt;English</A></CENTER></H1>
<HR>
<DIV id="term_area"><I><P>
This page needs no Internet-access to run; if you wish,
you can save a copy on your local disc and use it off-line.<BR>
Cette page n'a pas besoin d'acc&egrave;s &agrave; Internet pour fonctionner;
sauvegardez donc une copie locale pour l'employer hors-ligne.
EOT
print '&nbsp; Version:&nbsp;',datestamp();
print <<'EOT';
</P><P>
Just type in a word (in french (no accents unfortunately) or english)
followed by Enter . . .<BR>
Tapez simplement un mot (en fran&ccedil;ais (pas d'accents :-( ) ou en anglais)
suivi par Retour . . .<BR>
</P></I></DIV>
<HR>
<SCRIPT LANGUAGE="JavaScript">
window.focus();  // should prevent tabbing
var term_area = document.getElementById('term_area');
var search_string = '';
var new_para;
function search_and_display(argument) {
	argument = argument.replace(/Q/g,"'");
	argument = argument.replace(/A/g,"&");
	new_para = document.createElement('p');
	term_area.appendChild(new_para);
	var search_result = look_for_word(argument);
	new_para.innerHTML=search_string+"<BR>"+search_result.join("<BR>");
	window.scrollBy(0,10000);
	return void 0;   // don't trigger page-reload
}
// The DOM3 standard abandons all hope of creating order among event.keyCode,
// event.which and event.charCode, and instead defines new values for
// keydown and keyup events, event.keyIdentifier and event.keyLocation.
// The keyIdentifier is a string that in most cases looks like "U+0041"
// where the "0041" part is the unicode value of the character sent
// by the key when it is typed without modifiers,
// http://www.w3.org/TR/DOM-Level-3-Events/keyset.html#KeySet-Set
// So... could write a getc(e) returning ascii or x-notation, iso or utf8
function keyDown(argument) {
	var e = argument || window.event;  // window.event is IE
	if (search_string == '') {
		new_para = document.createElement('p');
		term_area.appendChild(new_para);
	}
	// if (true || e.keyIdentifier) {
	if (false && e.keyIdentifier) {
		// var k = e.keyIdentifier;
		var k = 'U+0042';
		alert('k = '+k);
		if (k.length() == 1) { // Konqueror returns ascii :-(
			
		}
		if (k.substr(0,2) == 'U+') {
			var u = parseInt('0x'+k.substr(-4,4));
			alert('u = '+u);
		}
	}
	var i = e.keyCode;
	// alert('i = '+i);
	if (i>=112 && i<=123) { return true; } // ignore Function-keys
	if (i>=35 && i<=40)   { return true; } // Home, End, Arrowkeys
	// but if we had a cursor, could allow Left & Right within the string...
	if (e.ctrlKey) { return true; }  // allow ^R ^S ^P ^W ^Q ^+ ^-
	// asciify what can be asciified
	if (i==109 || i==189) {
		i = 45;   // keyCode for -
	} else if (i>=65 && i<=90) {  // in ascii it's broader than A-Z...
		// if (e.ctrlKey) { i -= 64; }
		if (! e.shiftKey) { i += 32; }
	}
	if (! is_wanted(i)) { return true; }
	if (e.stopPropagation) { e.stopPropagation(); }
	if (e.preventDefault)  { e.preventDefault();
	} else {
        e.returnValue = false;   // IE
        e.cancelBubble = true;
    }

	if (i == 13) {
		if (search_string == '') { return false; }
		var search_result = look_for_word(search_string);
		if (search_result.length == 0) {
			new_para.innerHTML = search_string+"<BR> &nbsp; ?";
		} else {
			var j = 0;
			while (j < search_result.length) {
				search_result[j]=search_result[j].replace(/-([a-z]+)- suffix/,
				 '<A HREF="grammar_eo.html#suffixes">-$1- suffix</A>');
				j++;
			}
			new_para.innerHTML=search_string+"<BR>"+search_result.join("<BR>");
		}
		window.scrollBy(0,10000);
		search_string = '';
		
	} else if (i == 8) {
		search_string = search_string.replace(/.$/, '');
		new_para.innerHTML = search_string;
	} else {
		search_string += String.fromCharCode(i);
		new_para.innerHTML = search_string;
		window.scrollBy(0,10000);
	}
}
function is_wanted(i) {   // assumes already in ascii form
	// see http://unixpapa.com/js/key.html
	if (i>=65 && i<=90)  { return true; }  // A-Z
	if (i>=97 && i<=122) { return true; }  // a-z
	if (i==32 || i==13 || i==8)  { return true; }  // space enter backspace
	if (i==45) { return true; }  // -
	return false;
}
//if (window.addEventListener) {
	//3) useCapture: Boolean indicating whether to bind the event as it
	//  is propogating towards the target node, (event Capture), or as
	//  the event bubbles upwards from the target (event bubble).
	//  Set to true or false, respectively.
//	window.addEventListener('keydown',  keyDown,  false);
//} else if (window.attachEvent) {
	// alert('window.attachEvent');
	// IE5+ proprietary equivalent of addEventListener(). For the
	//  parameter eventType, the event string should include the "on"
	//  prefix (ie: "onload", "onclick" etc).
//	window.attachEvent('onkeydown',  keyDown);
//}
function init(){
  if(document.captureEvents) { document.captureEvents(Event.KEYDOWN); }
  document.onkeydown  = keyDown;
}
window.onload=init;
</SCRIPT>
</BODY></HTML>
EOT
